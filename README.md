# Coneron-back-end

Coneron-back-end repository includes Feathers (Node.js) based back-end of bid management platform for construction industry.

### WIP Demo:[coneron.gitlab.io](http://coneron.gitlab.io)

Sign in with the username "test1@test.com" and the password "test".


#### Front-end repository
- [Coneron-front-end](https://gitlab.com/coneron/coneron.gitlab.io)

## Back-end

- **[Feathers](https://feathersjs.com/)**
- **[Knex.js](http://knexjs.org/)** - Migration management
- **[Objection.js](http://vincit.github.io/objection.js)** - ORM
- **[feathers-objection](https://github.com/mcchrish/feathers-objection)** - Objection.js feathers adapter

### Continuous integration
- Gitlab CI deploying to [AWS EC2](https://aws.amazon.com/documentation/ec2/)
- AWS CodePipeline and Codedeploy are utilized to complete the deploy as documented by [this guide](https://gitlab.com/autronix/gitlabci-ec2-deployment-samples-guide/).
- AWS Parameter store is utilized for sensitive parameters
- Database for Demo Hosted on [AWS RDS] (https://aws.amazon.com/documentation/rds/)

#### Feathers Documentation

This project was bootstrapped with [Feathers](https://feathersjs.com/).

Refer Feathers guide how to perform common tasks.<br>
You can find the most recent version of guide [here](https://docs.feathersjs.com/).

