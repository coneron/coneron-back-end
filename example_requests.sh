#!/bin/sh

##############
### Insert ###
##############

# Add organizations
 curl 'http://localhost:3030/organizations/' -H 'Content-Type: application/json' --data-binary '{ "organizationName": "Jannen betoni Oy", "organizationIdNumber": "2423432" }'
 curl 'http://localhost:3030/organizations/' -H 'Content-Type: application/json' --data-binary '{ "organizationName": "Maskun ikkuna Oy", "organizationIdNumber": "2423433" }'

# Add organizationsTypes
 curl 'http://localhost:3030/organization-types/' -H 'Content-Type: application/json' --data-binary '{ "organizationTypeName": "Rakennusliike" }'
 curl 'http://localhost:3030/organization-types/' -H 'Content-Type: application/json' --data-binary '{ "organizationTypeName": "Alihankkija" }'

# # Add user
curl 'http://localhost:3030/users/' -H 'Content-Type: application/json' --data-binary '{ "email": "test1@test.com", "firstName": "Testi", "lastName": "Testinen", "password": "test", "organizationId": 1 }'
curl 'http://localhost:3030/users/' -H 'Content-Type: application/json' --data-binary '{ "email": "test2@test.com", "password": "test", "organizationId": 2 }'

# # Add project
 curl 'http://localhost:3030/projects/' -H 'Content-Type: application/json' --data-binary '{ "projectName": "Jannen esimerkki", "projectNumber": "2423425", "organizationId": 1 }' 
 curl 'http://localhost:3030/projects/' -H 'Content-Type: application/json' --data-binary '{ "projectName": "Maskun projekti", "projectNumber": "2423426", "organizationId": 2 }' 

# # Add quote
curl 'http://localhost:3030/quotes/' -H 'Content-Type: application/json' --data-binary '{ "quoteName": "Esimerkki pyyntö Jannen projektissa", "description": "Hyvä esimerkki", "projectId": 1 }'
curl 'http://localhost:3030/quotes/' -H 'Content-Type: application/json' --data-binary '{ "quoteName": "Esimerkki pyyntö Maskun projektissa", "description": "Hyvä esimerkki", "projectId": 2 }' 
 
# # Add contact
 curl 'http://localhost:3030/contacts/' -H 'Content-Type: application/json' --data-binary '{ "quoteId": 1, "email": "test1@test.com" }'
 curl 'http://localhost:3030/contacts/' -H 'Content-Type: application/json' --data-binary '{ "quoteId": 2, "email": "test1@test.com" }'  

# # Add offer  
 curl 'http://localhost:3030/offers/' -H 'Content-Type: application/json' --data-binary '{"message": "Hello, is it me you are looking for", "quoteId": 1 }'
 curl 'http://localhost:3030/offers/' -H 'Content-Type: application/json' --data-binary '{"message": "It must be me that you are looking for", "quoteId": 1 }'

# # Add service-categories  
 curl 'http://localhost:3030/service-categories/' -H 'Content-Type: application/json' --data-binary '{"serviceCategoryName": "sähkötyöt" }'

## Post file
 curl 'http://localhost:3030/uploads/' -H 'Content-Type: application/json' --data-binary '{ "uri": "data:image/gif;base64,R0lGODlhEwATAPcAAP/+//7/////+////fvzYvryYvvzZ/fxg/zxWfvxW/zwXPrtW/vxXvfrXv3xYvrvYvntYvnvY/ruZPrwZPfsZPjsZfjtZvfsZvHmY/zxavftaPrvavjuafzxbfnua/jta/ftbP3yb/zzcPvwb/zzcfvxcfzxc/3zdf3zdv70efvwd/rwd/vwefftd/3yfPvxfP70f/zzfvnwffvzf/rxf/rxgPjvgPjvgfnwhPvzhvjvhv71jfz0kPrykvz0mv72nvblTPnnUPjoUPrpUvnnUfnpUvXlUfnpU/npVPnqVPfnU/3uVvvsWPfpVvnqWfrrXPLiW/nrX/vtYv7xavrta/Hlcvnuf/Pphvbsif3zk/zzlPzylfjuk/z0o/LqnvbhSPbhSfjiS/jlS/jjTPfhTfjlTubUU+/iiPPokfrvl/Dll/ftovLWPfHXPvHZP/PbQ/bcRuDJP/PaRvjgSffdSe3ddu7fge7fi+zkuO7NMvPTOt2/Nu7SO+3OO/PWQdnGbOneqeneqvDqyu3JMuvJMu7KNfHNON7GZdnEbejanObXnOW8JOa9KOvCLOnBK9+4Ku3FL9ayKuzEMcenK9e+XODOiePSkODOkOW3ItisI9yxL+a9NtGiHr+VH5h5JsSfNM2bGN6rMJt4JMOYL5h4JZl5Jph3Jpl4J5h5J5h3KJl4KZp5Ks+sUN7Gi96lLL+PKMmbMZt2Jpp3Jpt3KZl4K7qFFdyiKdufKsedRdm7feOpQN2QKMKENrpvJbFfIrNjJL1mLMBpLr9oLrFhK69bJFkpE1kpFYNeTqFEIlsoFbmlnlsmFFwpGFkoF/////7+/v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAANAALAAAAAATABMAAAj/AKEJHCgokKJKlhThGciQYSIva7r8SHPFzqGGAwPd4bKlh5YsPKy0qFLnT0NAaHTcsIHDho0aKkaAwGCGEkM1NmSkIjWLBosVJT6cOjUrzsBKPl54KmYsACoTMmk1WwaA1CRoeM7siJEqmTIAsjp40ICK2bEApfZcsoQlxwxRzgI8W8XhgoVYA+Kq6sMK0QEYKVCUkoVqQwQJFTwFEAAAFZ9PlFy4OEEiRIYJD55EodDA1ClTbPp0okRFxBQDBRgskAKhiRMlc+Sw4SNpFCIoBBwkUMBkCBIiY8qAgcPG0KBHrBTFQbCEV5EjQYQACfNFjp5CgxpxagVtUhIjwzaJYSHzhQ4cP3ryQHLEqJbASnu+6EIW6o2b2X0ISXK0CFSugazs0YYmwQhziyuE2PLLIv3h0hArkRhiCCzAENOLL7tgAoqDGLXSSSaPMLIIJpmAUst/GA3UCiuv1PIKLtw1FBAAOw==" }'  


###########
### Get ###
###########

# Get all projects
#curl 'http://localhost:3030/projects/'

# Get current users info
# Get users organization and save it


# Get organizations the user 1 belongs to
curl 'http://localhost:3030/users?id=1'

# Get all projects in organization 1
curl 'http://localhost:3030/projects?organizationId=1'

# Get all projects in organization 2
curl 'http://localhost:3030/projects?organizationId=2'

# Get all quotes in project 1
curl 'http://localhost:3030/quotes?projectId=1'


# Get all files in quote


