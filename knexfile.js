require('dotenv').config();
const path = require('path');

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'coneron_back_end'
    },
    migrations: {
      directory: path.join(__dirname, 'migrations'),
      tableName: 'migrations'
    },
    seeds: {
      directory: __dirname + '/seeds/development'
    }
  },

  test: {
    client: 'postgresql',
    connection: {
      database: 'coneron_back_end_test'
    },
    migrations: {
      directory: path.join(__dirname, 'migrations'),
      tableName: 'migrations'
    },
    seeds: {
      directory: __dirname + '/seeds/test'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'coneron_back_end',
      user: 'get_from_dotenv',
      password: 'get_from_dotenv'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'pg',
    connection: {
      host : process.env.DATABASE_HOST,
      database: process.env.DATABASE_NAME,
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      port: '5432',
    },
    pool: {
      min: 2,
      max: 10
    },
    seeds: {
      directory: __dirname + '/db/seeds/production'
    },
    migrations: {
      directory: path.join(__dirname, 'migrations'),      
      tableName: 'migrations'
    }
  }

};
