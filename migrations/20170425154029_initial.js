
exports.up = function (knex) {
  console.log('Creating initial migration');

  // TODO: Add indexes
  return knex.schema
    .createTableIfNotExists('organizationTypes', table => {
      table.bigIncrements('id').primary();
      table.string('organizationTypeName').notNullable();
    })

    .createTableIfNotExists('organizations', table => {
      table.bigIncrements('id').primary();
      table.string('organizationName').notNullable();
      table.string('organizationIdNumber');
      table.integer('organizationTypeId').unsigned().index(); //.notNullable()       
      table.foreign('organizationTypeId')
        .references('id')
        .inTable('organizationTypes')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
    })

    .createTableIfNotExists('users', function (table) {
      table.bigIncrements('id').primary();
      table.string('email').unique();
      table.string('password');
      table.string('firstName');
      table.string('lastName');      
      table.boolean('isVerified');
      table.string('verifyToken');
      table.string('verifyShortToken');
      table.bigInteger('verifyExpires');
      table.jsonb('verifyChanges');
      table.string('resetToken');
      table.string('resetShortToken');
      table.bigInteger('resetExpires');
      table.integer('organizationId').unsigned().index(); //.notNullable()      
      table.foreign('organizationId')
        .references('id')
        .inTable('organizations')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
    })
    
    .createTableIfNotExists('projects', table => {
      table.bigIncrements('id').primary();
      table.string('projectName').notNullable();
      table.string('projectNumber');
      table.boolean('isArchived');
      table.integer('organizationId').unsigned().notNullable().index();      
      table.foreign('organizationId')
        .references('id')
        .inTable('organizations')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
    })
    
    .createTableIfNotExists('quotes', table => {
      table.bigIncrements('id').primary();
      table.string('quoteName'); // .notNullable()
      table.string('description');
      table.timestamp('quoteEndDate');
      table.integer('projectId').unsigned().index(); //.notNullable()
      table.foreign('projectId')
        .references('id')
        .inTable('projects')
        .onDelete('CASCADE')
        .onUpdate('CASCADE');
    })

    .createTableIfNotExists('serviceCategories', table => {
      table.bigIncrements('id').primary();
      table.string('serviceCategoryName').notNullable();
    })
    
    .createTableIfNotExists('contacts', table => {
      table.bigIncrements('id').primary();
      table.integer('userId').unsigned().index();
      table.string('email');
      table.foreign('userId')
        .references('id')
        .inTable('users')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
      table.integer('quoteId').unsigned().index();
      table.foreign('quoteId')
        .references('id')
        .inTable('quotes')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
    })
    
    .createTableIfNotExists('files', table => {
      table.bigIncrements('id').primary();
      table.string('fileName').notNullable();
      table.string('fileId');
      table.integer('quoteId').unsigned().index();
      table.foreign('quoteId')
        .references('id')
        .inTable('quotes')
        .onDelete('CASCADE')
        .onUpdate('CASCADE');
    })
    
    .createTableIfNotExists('offers', table => {
      table.bigIncrements('id').primary();
      table.string('message');
      table.integer('quoteId').unsigned().index();
      table.foreign('quoteId')
        .references('id')
        .inTable('quotes')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
      table.integer('organizationId').unsigned().index();
      table.foreign('organizationId')
        .references('id')
        .inTable('organizations')
        .onDelete('RESTRICT')
        .onUpdate('CASCADE');
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('offers')
    .dropTableIfExists('files')
    .dropTableIfExists('contacts')
    .dropTableIfExists('serviceCategories')
    .dropTableIfExists('quotes')
    .dropTableIfExists('projects')
    .dropTableIfExists('users')
    .dropTableIfExists('organizations')
    .dropTableIfExists('organizationTypes');        
};
