#!/bin/sh
source /home/$USER/.bash_profile
APP="ConeronCodePipeline"
pm2 describe ${APP} > /dev/null
RUNNING=$?

export SPARKPOST_API_KEY=$(aws ssm get-parameters --region eu-west-1 --names SPARKPOST_API_KEY --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//') 
export GMAIL=$(aws ssm get-parameters --region eu-west-1 --names GMAIL --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export GMAIL_PASSWORD=$(aws ssm get-parameters --region eu-west-1 --names GMAIL_PASSWORD --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export DATABASE_HOST=$(aws ssm get-parameters --region eu-west-1 --names DATABASE_HOST --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export DATABASE_NAME=$(aws ssm get-parameters --region eu-west-1 --names DATABASE_NAME --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export DATABASE_USER=$(aws ssm get-parameters --region eu-west-1 --names DATABASE_USER --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export DATABASE_PASSWORD=$(aws ssm get-parameters --region eu-west-1 --names DATABASE_PASSWORD --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')
export AUTH_SECRET=$(aws ssm get-parameters --region eu-west-1 --names AUTH_SECRET --with-decryption --query Parameters[0].Value | sed -e 's/^"//' -e 's/"$//')

export PORT=8080
export NODE_ENV=production

if [ "${RUNNING}" -ne 0 ]; then
  cd /usr/local/projects/source/; pm2 start ecosystem.config.js
else
  pm2 reload ${APP} --log-date-format="YYYY-MM-DD HH:mm Z"
fi
