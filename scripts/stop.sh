#!/bin/sh
source /home/$USER/.bash_profile
APP="ConeronCodePipeline"
pm2 describe ${APP} > /dev/null
RUNNING=$?

if [ "${RUNNING}" -eq 0 ]; then
 pm2 delete ${APP}
fi
