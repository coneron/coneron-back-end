exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('organizations')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('organizations').insert([
        {
          organizationName: 'Jannen betoni Oy',
          organizationIdNumber: '2423432'
        },
        {
          organizationName: 'Maskun ikkuna Oy',
          organizationIdNumber: '2423433'
        }
      ]);
    })
    .then(function() {
      return knex('projects')
      .del()
      .then(function() {
        // Inserts seed entries
        return knex('projects').insert([
          {
            projectName: 'Jannen esimerkki',
            projectNumber: '2423425',
            organizationId: 1
          },
          {
            projectName: 'Maskun projekti',
            projectNumber: '2423426',
            organizationId: 2
          }
        ]);
      });
    });
};
