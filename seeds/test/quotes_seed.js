exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('quotes').del().then(function() {
    // Inserts seed entries
    return knex('quotes').insert([
      {
        quoteName: 'Esimerkki pyyntö Jannen projektissa',
        description: 'Hyvä esimerkki',
        projectId: 1
      },
      {
        quoteName: 'Esimerkki pyyntö Maskun projektissa',
        description: 'Hyvä esimerkki',
        projectId: 2
      }
    ]);
  });
};
