const logger = require('winston');
const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');

const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const socketio = require('feathers-socketio');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');

const authentication = require('./authentication');

const postgres = require('./postgres');

const app = feathers();


// Load app configuration
const conf = configuration(path.join(__dirname, '..'));
app.configure(conf);

// logger.info('GMail:', process.env.GMAIL);
// logger.info('Config dir:', process.env.NODE_CONFIG_DIR);
// logger.info('Config:', conf());

// logger.info('Database pass:', process.env.DATABASE_PASSWORD);
// logger.info('Database user:', process.env.DATABASE_USER);
// logger.info('Database host:', process.env.DATABASE_HOST);
// logger.info('Database name:', process.env.DATABASE_NAME);


// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', feathers.static(app.get('public')));

// TODO: Remove and host elsewhere
app.use('/temps', feathers.static(__dirname + '/services/uploads/temp_uploads')); //temps

// Set up Plugins and providers
app.configure(hooks());
app.configure(postgres);
app.configure(rest());
app.configure(socketio());

app.configure(authentication);

// Set up our services (see `services/index.js`)
app.configure(services);
// Configure middleware (see `middleware/index.js`) - always has to be last
app.configure(middleware);
app.hooks(appHooks);

module.exports = app;
