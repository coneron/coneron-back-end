/* eslint-disable no-console */
const Model = require('objection').Model;

class Contacts extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'contacts';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id: {type: 'integer'},
        email: {type: 'string'},
        userId: {type: 'integer'},
        quoteId: {type: 'integer'},
      }
    };
  }
  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      quote: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/quotes.model',
        join: {
          from: 'contacts.quoteId',
          to: 'quotes.id'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/users.model',
        join: {
          from: 'contacts.userId',
          to: 'users.id'
        }
      },
    };
  }

}

module.exports = Contacts;

