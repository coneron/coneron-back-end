/* eslint-disable no-console */

// files-model.js - A Objection.js model
// 
// See http://vincit.github.io/objection.js/
// for more of what you can do here.
const Model = require('objection').Model;

class files extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'files';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id: {type: 'integer'},
        fileName: {type: 'string'},
        fileId: {type: 'string'},
        quoteId: {type: 'integer'},
      }
    };
  }

}

module.exports = files;

