/* eslint-disable no-console */

// offers-model.js - A Objection.js model
// 
// See http://vincit.github.io/objection.js/
// for more of what you can do here.
const Model = require('objection').Model;

class offers extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'offers';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id: {type: 'integer'},
        message: {type: 'string'},
        quoteId: {type: 'integer'},
        organizationId: {type: 'integer'},
      }
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      quote: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/quotes.model',
        join: {
          from: 'offers.quoteId',
          to: 'quotes.id'
        }
      },
    };
  }

}

module.exports = offers;

