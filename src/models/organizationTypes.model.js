/* eslint-disable no-console */
const Model = require('objection').Model;

class organizationTypes extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'organizationTypes';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
    };
  }

}

module.exports = organizationTypes;
