/* eslint-disable no-console */
const Model = require('objection').Model;

class Organizations extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'organizations';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['organizationName'],

      properties: {
        id: {type: 'integer'},
        organizationName: {type: 'string'},
        organizationIdNumber: {type: 'string'},
        organizationTypeId: {type: 'integer'},
      }
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      users: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/users.model',
        join: {
          from: 'organizations.id',
          to: 'users.organizationId'
        }
      },
      organizationsProjects: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/projects.model',
        join: {
          from: 'organizations.id',
          to: 'projects.organizationId'
        }
      },
    };
  }
}

module.exports = Organizations;
