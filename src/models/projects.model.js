/* eslint-disable no-console */
const Model = require('objection').Model;

class Projects extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'projects';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['projectName'],

      properties: {
        id: {type: 'integer'},
        projectName: {type: 'string'},
        projectNumber: {type: ['string', 'null']},
        isArchived: {type: ['boolean', 'null']},        
        organizationId: {type: 'integer'},
/*      createdAt: {type:'timestamp'},
        updatedAt: {type:'timestamp'},*/
      }
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      organizations: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/organizations.model',
        join: {
          from: 'projects.organizationId',
          to: 'organizations.id'
        }
      },
      quotes: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/quotes.model',
        join: {
          from: 'projects.id',
          to: 'quotes.projectId'
        }
      },

    };
  }
}

module.exports = Projects;

