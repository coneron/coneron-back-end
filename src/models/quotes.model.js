/* eslint-disable no-console */
const Model = require('objection').Model;


class Quotes extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'quotes';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      
// TODO: Add validation with error message
//       properties: {
//         id: {type: 'integer'},
//         quoteName: {type: 'string'},
//         description: {type: 'string'},
//         quoteEndDate: {type: 'timestamp'},
//         projectId: {type: 'integer'},
// /*      createdAt: {type:'timestamp'},
//         updatedAt: {type:'timestamp'},*/
//       }
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      projects: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/projects.model',
        join: {
          from: 'quotes.projectId',
          to: 'projects.id'
        }
      },
      contacts: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/contacts.model',
        join: {
          from: 'quotes.id',
          to: 'contacts.quoteId'
        }
      },
      offers: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/offers.model',
        join: {
          from: 'quotes.id',
          to: 'offers.quoteId'
        }
      }
    };
  }
  
}

module.exports = Quotes;

