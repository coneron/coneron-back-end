/* eslint-disable no-console */
const Model = require('objection').Model;

class Users extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'users';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email', 'password'],

      // properties: {
      //   id: { type: 'integer' },
      //   email: { type: 'string' },
      //   password: { type: 'string' },
      //   organizationId: { type: 'integer' },
      //   isVerified: { type: 'boolean' },
      //   verifyToken: { type: 'string' },
      //   verifyExpires: { type: 'integer' },
      //   verifyChanges: { type: 'object' },
      //   resetToken: { type: 'string' },
      //   resetShortToken: { type: 'string' },
      //   resetExpires: { type: 'integer' }
      // }
    };
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {
      organization: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/organizations.model',
        join: {
          from: 'users.organizationId',
          to: 'organizations.id'
        }
      },
      contacts: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/contacts.model',
        join: {
          from: 'users.id',
          to: 'contacts.userId'
        }
      },

    };
  }
}

module.exports = Users;