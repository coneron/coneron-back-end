const knex = require('knex');
const Model = require('objection').Model;

const environment = process.env.NODE_ENV || 'development';
const config = require('../knexfile.js')[environment];

module.exports = function () {
  const app = this;
  const db = knex(config);

  Model.knex(db);

  app.set('knexClient', db);
};
