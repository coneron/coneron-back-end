const isProd = process.env.NODE_ENV === 'production';
const path = require('path');
const jade = require('jade');

module.exports = function(app) {
  const returnEmail = process.env.GMAIL;

  function getLink(type, hash) {
    var port =
      app.get('clientPort') === '80' || isProd
        ? ''
        : ':' + app.get('clientPort');
    var host = app.get('host') === 'HOST' ? 'localhost' : app.get('host');
    var protocal =
      app.get('protocal') === 'PROTOCAL' ? 'http' : app.get('protocal');
    protocal += '://';
    return `${protocal}${host}${port}/#/${type}/${hash}`;
  }

  function sendEmail(email) {
    return app
      .service('email')
      .create(email)
      .then(function(result) {
        console.log('Sent email', result);
      })
      .catch(err => {
        console.log('Error sending email', err);
      });
  }

  return {
    getLink,
    notifier: function(type, user, notifierOptions) {
      console.log(`-- Preparing email for ${type}`);
      var hashLink;
      var email;
      var emailAccountTemplatesPath = path.join(
        './',
        'src',
        'email-templates',
        'account'
      );
      var templatePath;
      var compiledHTML;

      switch (type) {
      case 'resendVerifySignup': // send another email with link for verifying user's email addr
        hashLink = getLink('verify', user.verifyToken);

        templatePath = path.join(
            emailAccountTemplatesPath,
            'verify-email.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Confirm Signup',
          html: compiledHTML
        };

        return sendEmail(email);

      case 'verifySignup': // inform that user's email is now confirmed
        hashLink = getLink('verify', user.verifyToken);

        templatePath = path.join(
            emailAccountTemplatesPath,
            'email-verified.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Thank you, your email has been verified',
          html: compiledHTML
        };

        return sendEmail(email);

      case 'sendResetPwd': // inform that user's email is now confirmed
        hashLink = getLink('reset', user.resetToken);

        templatePath = path.join(
            emailAccountTemplatesPath,
            'reset-password.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Reset Password',
          html: compiledHTML
        };

        return sendEmail(email);

      case 'resetPwd': // inform that user's email is now confirmed
        hashLink = getLink('reset', user.resetToken);

        templatePath = path.join(
            emailAccountTemplatesPath,
            'password-was-reset.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Your password was reset',
          html: compiledHTML
        };

        return sendEmail(email);

      case 'passwordChange':
        templatePath = path.join(
            emailAccountTemplatesPath,
            'password-change.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          returnEmail
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Your password was changed',
          html: compiledHTML
        };

        return sendEmail(email);

      case 'identityChange':
        hashLink = getLink('verifyChanges', user.verifyToken);

        templatePath = path.join(
            emailAccountTemplatesPath,
            'identity-change.jade'
          );

        compiledHTML = jade.compileFile(templatePath)({
          logo: '',
          name: user.name || user.email,
          hashLink,
          returnEmail,
          changes: user.verifyChanges
        });

        email = {
          from: process.env.GMAIL,
          to: user.email,
          subject: 'Your account was changed. Please verify the changes',
          html: compiledHTML
        };

        return sendEmail(email);

      default:
        break;
      }
    }
  };
};
