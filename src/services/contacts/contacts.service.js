// Initializes the `contacts` service on path `/contacts`
const ObjectionService = require('feathers-objection');
const contacts = require('../../models/contacts.model');
const hooks = require('./contacts.hooks');
const filters = require('./contacts.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: contacts,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/contacts', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('contacts');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
