// Initializes the `files` service on path `/files`
const ObjectionService = require('feathers-objection');
const files = require('../../models/files.model');
const hooks = require('./files.hooks');
const filters = require('./files.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: files,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/files', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('files');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
