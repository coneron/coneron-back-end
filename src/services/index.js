const users = require('./users/users.service.js');
const projects = require('./projects/projects.service.js');
const quotes = require('./quotes/quotes.service.js');
const serviceCategories = require('./serviceCategories/serviceCategories.service.js');
const organizations = require('./organizations/organizations.service.js');
const organizationTypes = require('./organizationTypes/organizationTypes.service.js');
const contacts = require('./contacts/contacts.service.js');
const uploads = require('./uploads/uploads.service.js');
const files = require('./files/files.service.js');
const mailQuote = require('./mail-quote/mail-quote.service.js');
const offers = require('./offers/offers.service.js');
const authManagement = require('./auth-management/auth-management.service.js');
const email = require('./email/email.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(projects);
  app.configure(quotes);
  app.configure(serviceCategories);
  app.configure(organizations);
  app.configure(organizationTypes);  
  app.configure(contacts);
  app.configure(uploads);
  app.configure(files);
  app.configure(mailQuote);
  app.configure(offers);
  app.configure(authManagement);
  app.configure(email);
};