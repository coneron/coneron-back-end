// const { authenticate } = require('feathers-authentication').hooks;
// const isProd = process.env.NODE_ENV === 'production'
const logger = require('winston');
const indicative = require('indicative');
const accountService = require('../auth-management/notifier');

module.exports = {
  before: {
    all: [], // TODO: authenticate('jwt') 
    find: [],
    get: [],
    create: [
      function (hook) {
        logger.info('Hook data:', hook.data);
        logger.info('Hook app host:', hook.app.get('host'));

        const resultLink = accountService(hook.app).getLink('quote', hook.data.id);

        let contacts = [];
        const rules = {
          'contacts': 'array|min:1',
          'contacts.*.email': 'required|email'
        };

        return indicative
          .validate(hook.data, rules)
          .then(function () {
            contacts = hook.data.contacts.map(contact => contact.email);

            hook.data = {
              from: 'coneron.org@gmail.com',
              to: contacts,
              subject: 'Uusi tarjouspyyntö',
              html: 'Avaa tarjouspyyntö tästä linkistä : ' + resultLink
            };

            logger.info('Hook data:', hook.data);
            return hook;

          })
          .catch(function (errors) {
            logger.error('Errors data:', errors);
            throw new Error(errors);
          });
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      function (hook) {
        logger.info('Hook data in after:', hook.data);
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function (hook) {
        logger.error(`Error in '${hook.path}' service method '${hook.method}`, hook.error.errors);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
