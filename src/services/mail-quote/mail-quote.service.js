// Initializes the `mail.quote` service on path `/mail-quote`
// const createService = require('./mail-quote.class.js');
const Mailer = require('feathers-mailer');
const hooks = require('./mail-quote.hooks');
const filters = require('./mail-quote.filters');
const smtpTransport = require('nodemailer-smtp-transport');
// const sparkPostTransport = require('nodemailer-sparkpost-transport');

module.exports = function () {
  const app = this;
 
  // Register the service, see below for an example
  app.use('/mail-quote', Mailer(smtpTransport({
    service: 'gmail',
    auth: {
      user: app.get('GMAIL'),
      pass: app.get('GMAIL_PASSWORD')
    }
  })));

  // TODO: Transfer back to sparkPost after domain is resolved

  // app.use('/mail-quote', Mailer(sparkPostTransport({
  //   'sparkPostApiKey': process.env.SPARKPOST_API_KEY,
  //   'options': {
  //     'open_tracking': true,
  //     'click_tracking': true,
  //     'transactional': true,
  //     'sandbox': true,
  //   },
  // })));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('mail-quote');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
