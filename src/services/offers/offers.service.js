// Initializes the `offers` service on path `/offers`
const ObjectionService = require('feathers-objection');
const offers = require('../../models/offers.model');
const hooks = require('./offers.hooks');
const filters = require('./offers.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: offers,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/offers', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('offers');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
