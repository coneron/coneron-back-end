// Initializes the `contacts` service on path `/contacts`
const ObjectionService = require('feathers-objection');
const organizationTypes = require('../../models/organizationTypes.model');
const hooks = require('./organizationTypes.hooks');
const filters = require('./organizationTypes.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: organizationTypes,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/organization-types', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('organization-types');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
