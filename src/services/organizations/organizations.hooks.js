//const { authenticate } = require('feathers-authentication').hooks;
//const Organizations = require('../../models/organizations.model');
const { populate } = require('feathers-hooks-common');

const organizationUsersSchema = {
  include: {
    service: 'users',
    nameAs: 'users',
    parentField: 'id',
    childField: 'organizationId',
  }
};

module.exports = {
  before: {
    all: [ ], // TODO: authenticate('jwt') 
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [populate({ schema: organizationUsersSchema })],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
