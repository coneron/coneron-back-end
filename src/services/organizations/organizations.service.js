// Initializes the `organizations` service on path `/organizations`
const organizations = require('../../models/organizations.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./organizations.hooks');
const filters = require('./organizations.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: organizations,
    id: 'id',
    paginate,
    allowedEager: '[users]',
  };

  // Initialize our service with any options it requires
  app.use('/organizations', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('organizations');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
