//const { authenticate } = require('feathers-authentication').hooks;
//const logger = require('winston');
const Projects = require('../../models/projects.model');

module.exports = {
  before: {
    all: [], //TODO: authenticate('jwt') 
    find: [
      function(hook) {
        if(hook.params.query.organizationId) {
          return Projects
          .query()
          .where('organizationId', hook.params.query.organizationId)                  
          .eager('quotes')
          .then(projects => {
                      
            projects.map(project => project.quotes = project.quotes.length);              

            const options =  {
              'limit': 10,
              'data': projects,
            };

            hook.result = options;
            return hook;
          });
        }
      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
