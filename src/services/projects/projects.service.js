// Initializes the `projects` service on path `/projects`
const projects = require('../../models/projects.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./projects.hooks');
const filters = require('./projects.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: projects,
    id: 'id',
    paginate,
    allowedEager: '[quotes]',
  };

  // Initialize our service with any options it requires
  app.use('/projects', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('projects');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
