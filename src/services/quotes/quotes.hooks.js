// const { authenticate } = require('feathers-authentication').hooks;
const Contacts = require('../../models/contacts.model');
const Quotes = require('../../models/quotes.model');
const logger = require('winston');


module.exports = {
  before: {
    all: [], //TODO: authenticate('jwt')
    find: [
      function(hook) {
        logger.info('Hook params:', hook.params.query.projectId);
        logger.info('Hook moreParams:', hook.params.query.filter);        
        // TODO: User param check instead of id

        const paramFilter = hook.params.query.filter;
        
        if(hook.params.query.projectId) {
          return Quotes
          .query()
          .modify(function(queryBuilder) {
            logger.info('paramFilter', paramFilter);                    
            if (paramFilter) {
              queryBuilder.whereRaw('LOWER("quoteName") LIKE ?', [`%${paramFilter.toLowerCase()}%`]);
            }
          })          
          .eager('offers')
          .where('projectId', hook.params.query.projectId)
          .then(quotes => {

            quotes.map(quote => quote.offers = quote.offers.length);              

            const options =  {
              'limit': 10,
              'data': quotes,
            };
            hook.result = options;
            logger.info('Hook result:', hook.result);        
            
            return hook;
          });

        }else{
          return Quotes
          .query()
          .eager('projects.organizations')
          .then(quotes => {
            const options =  {
              'limit': 10,
              'data': quotes,
            };
            hook.result = options;
            return hook;
          });
        }        
        
      }
    ],
    get: [],
    create: [],
    update: [
      function(hook) {
        logger.info('Hook data on update:', hook.data);

        if(hook.result.id && hook.data.contacts.length > 0){
          hook.data.contacts.map((contact) => {
            return contact.quoteId = parseInt(hook.result.id, 10);
          });
          
          // TODO: Check if contacts exits & Add transaction
          Contacts
          .query()
          .insert(hook.data.contacts);

        }   
      }
    ],
    patch: [
      function(hook) {
        logger.info('Hook data on update:', hook.data);
      }
    ],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [
      function(hook) {
        logger.info('Hook data on get:', hook.result);

        // TODO: Throw error if not id
        return Contacts
          .query()
          .where('quoteId', hook.result.id)
          .then(contacts => {
            hook.result.contacts = contacts;
            return hook;
          })
          .catch(err => {
            logger.error(err);
          });

      }
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
