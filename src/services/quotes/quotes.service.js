// Initializes the `quotes` service on path `/quotes`
const quotes = require('../../models/quotes.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./quotes.hooks');
const filters = require('./quotes.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: quotes,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/quotes', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('quotes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
