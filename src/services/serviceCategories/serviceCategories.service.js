// Initializes the `contacts` service on path `/contacts`
const ObjectionService = require('feathers-objection');
const serviceCategories = require('../../models/serviceCategories.model');
const hooks = require('./serviceCategories.hooks');
const filters = require('./serviceCategories.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: serviceCategories,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/service-categories', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('service-categories');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
