// const { authenticate } = require('feathers-authentication').hooks;
const logger = require('winston');
const dauria = require('dauria');
const Files = require('../../models/files.model');


module.exports = {
  before: {
    all: [], // TODO: authenticate('jwt')
    find: [],
    get: [],
    create: [
      function(hook) {
        logger.info('Stored quote id', hook.data.quoteId);
        hook.quoteId = hook.data.quoteId;

        if (!hook.data.uri && hook.params.file){
          const file = hook.params.file;
          hook.fileName = file.originalname;

          const uri = dauria.getBase64DataURI(file.buffer, file.mimetype);
          hook.data = {uri: uri};
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      function(hook) {
        logger.info('Stored name ', hook.fileName);  
        logger.info('Stored blob with id', hook.result.id);
        logger.info('Stored quote id:', hook.quoteId);

        // logger.info('Hook whole data :', hook.result);
        
    
        const fileName = hook.fileName; 
        const fileId = hook.result.id;
        const quoteId = parseInt(hook.quoteId, 10) ;  
        const fileObject = {fileName, fileId, quoteId};

        logger.info('File object', fileObject);
        

        return Files
        .query()
          .insert(fileObject).then(response => {
            logger.info(response);
            hook.result.id = response.id;
            return hook;
          })
          .catch(err => {
            logger.error(err);
          });  

      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
