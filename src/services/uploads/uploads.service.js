// Initializes the `uploads` service on path `/uploads`
const blobService = require('feathers-blob');
const hooks = require('./uploads.hooks');
const filters = require('./uploads.filters');
const fs = require('fs-blob-store');
const multer = require('multer');
const multipartMiddleware = multer();

module.exports = function () {
  const app = this;

  const blobStorage = fs(__dirname + '/temp_uploads');

  // Initialize our service with any options it requires
  app.use('/uploads',

    // multer parses the file named 'uri'.
    // Without extra params the data is
    // temporarely kept in memory
    multipartMiddleware.single('uri'),

    // another middleware, this time to
    // transfer the received file to feathers
    function(req,res,next){
      req.feathers.file = req.file;
      next();
    },
    blobService({Model: blobStorage})
);

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('uploads');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
