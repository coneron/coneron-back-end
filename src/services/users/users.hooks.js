const logger = require('winston');
const { authenticate } = require('feathers-authentication').hooks;
const commonHooks = require('feathers-hooks-common');
const { restrictToOwner } = require('feathers-authentication-hooks');

const { hashPassword } = require('feathers-authentication-local').hooks;
const verifyHooks = require('feathers-authentication-management').hooks;
const sendVerificationEmail = require('../../hooks/send-verification-email');

const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: 'id',
    ownerField: 'id'
  })
];

module.exports = {
  before: {
    all: [],
    find: [ ], //TODO: authenticate('jwt')
    get: [ ...restrict ],
    create: [ hashPassword(), 
      verifyHooks.addVerification(),
      function (hook) {
        logger.log('Hook data after addVerification: ', hook.data);
      } 
    ],
    update: [ ...restrict, hashPassword() ],
    patch: [ hashPassword() ], // TODO: Add ...restrict
    remove: [ ...restrict ]
  },

  after: {
    all: [
      commonHooks.when(
        hook => hook.params.provider,
        commonHooks.discard('password')
      ),
    ],
    find: [],
    get: [],
    create: [
      sendVerificationEmail(),
      verifyHooks.removeVerification()
    ],
    update: [],
    patch: [
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
