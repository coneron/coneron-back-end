process.env.NODE_ENV = 'test';

var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
const server = require('../../src/app');
const knex = require('knex');

chai.use(chaiHttp);

const db = knex(require('../../knexfile.js')[process.env.NODE_ENV]);

describe('API Routes', function() {

  beforeEach(function(done) {
    db.migrate.rollback()
    .then(function() {
      db.migrate.latest()
      .then(function() {
        return db.seed.run()
        .then(function() {
          done();
        });
      });
    });
  });

  afterEach(function(done) {
    db.migrate.rollback()
    .then(function() {
      done();
    });
  });

  describe('GET /projects/', function() {
    it('expect return all projects', function(done) {
      chai.request(server).get('/projects/').end(function(err, res) {
        expect(res).to.be.status(200);
        expect(res).to.be.json;
        expect(res.body.data[0]).to.have.property('id');
        expect(res.body.data[0].id).to.equal('1');
        expect(res.body.data[0]).to.have.property('projectName');
        expect(res.body.data[0].projectName).to.equal('Jannen esimerkki');
        expect(res.body.data[0]).to.have.property('projectNumber');
        expect(res.body.data[0].projectNumber).to.equal('2423425');
        expect(res.body.data[0]).to.have.property('organizationId');
        expect(res.body.data[0].organizationId).to.equal(1);
        done();
      });
    });
  });
  describe('GET /quotes?projectId=1', () => {
    it('expect return quotes in project 1', (done) => {
      chai.request(server).get('/quotes?projectId=1').end(function(err, res) {
        expect(res).to.be.status(200);
        expect(res).to.be.json;
        expect(res.body.data[0]).to.have.property('id');
        expect(res.body.data[0].id).to.equal('1');
        expect(res.body.data[0]).to.have.property('description');
        expect(res.body.data[0].description).to.equal('Hyvä esimerkki');
        expect(res.body.data[0]).to.have.property('projectId');
        expect(res.body.data[0].projectId).to.equal(1);
        expect(res.body.data[1]).to.be.an('undefined');
        done();
      });
    });
  });

});
