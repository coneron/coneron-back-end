const assert = require('assert');
const app = require('../../src/app');

describe('\'mail.quote\' service', () => {
  it('registered the service', () => {
    const service = app.service('mail-quote');

    assert.ok(service, 'Registered the service');
  });
});
